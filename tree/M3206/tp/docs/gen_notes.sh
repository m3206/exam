#!/bin/bash

for rep in $(find . -type d -iname cours); do
    for sub in $(find $rep -type d -iname notes); do
        cat notes_gr1.csv | awk -v seed=$RANDOM 'BEGIN {FS=":"; i=0;srand(seed); } { printf("%d:%s:%s:%s:%d.%d0\n", $1, $2, $3, $4, rand()*20, int(rand()*2)*5 ); }' > test
        echo $rep $sub
        mv test $sub/evaluations.csv
    done
done

for rep in $(find . -type d -iname td); do
    for sub in $(find $rep -type d -iname notes); do
        cat notes_gr1.csv | awk -v seed=$RANDOM 'BEGIN {FS=":"; i=0;srand(seed); } { printf("%d:%s:%s:%s:%d.%d0:%d.%d0\n", $1, $2, $3, $4, rand()*20, int(rand()*2)*5, rand()*20, int(rand()*2)*5 ); }' > test
        echo $rep $sub
        mv test $sub/evaluations.csv
    done
done

for rep in $(find . -type d -iname tp); do
    for sub in $(find $rep -type d -iname notes); do
        cat notes_gr1.csv | awk -v seed=$RANDOM 'BEGIN {FS=":"; i=0;srand(seed); } { printf("%d:%s:%s:%s:%d.%d0:%d.%d0:%d.%d0\n", $1, $2, $3, $4, rand()*20, int(rand()*2)*5, rand()*20, int(rand()*2)*5 , rand()*20, int(rand()*2)*5); }' > test
        echo $rep $sub
        mv test $sub/evaluations.csv
    done
done
