## Season-Pre final TP pour M3206

__Attention, le sujet de l'examen se trouve ici, mais les réponses doivent être
complétées sur le moodle__

__Vérifiez bien que le numéro de réponse correspond au numéro des questions__


__Tous les fichiers sont générés aléatoirement__

<br><br><br><br>

### Etape 0: Conseils, outils et notes importantes

- Je vous conseille d'utiliser la machine virtuelle `Debian_Jessie_64bit.vdi` sur RT-CLOUD, 
    dans le répertoire `Machines Virtuelles/Debian/Debian_Minimal_Tahiry`. Ce point est très
    important car l'examen a été conçu avec cette machine. Si vous n'utilisez pas la machine
    `Debian_Jessie_64bit.vdi`, faites le à vos risques et péril. 
- Connectez vous en ssh à votre machine en utilisant `putty` ou un `terminal`. Ce point aussi
    est très important car vous allez devoir faire des copier/coller de vos résultats dans 
    le moodle. Si vous n'utilisez pas ssh, faites le à vos riques et périls.
- Créez un répertoire vide nommé: `/home/rt/examen`. Positionnez vous dans ce répertoire.
- Téléchargez l'archive ici: `wget https://gitlab.com/m3206/cours/raw/master/files/tree.tar.gz`.
    Si vous n'utilisez pas la commande wget, deplacer l'archive dans le répertoire `/home/rt/examen`.
- Une fois le fichier téléchargé, lancer la commande `shasum tree.tar.gz` ou `sha1sum tree.tar.gz`.
    vous devez obtenir le résultat suivant:
    
    ```bash
    $ shasum tree.tar.gz 
    3f47fc53c320d128f6274c5480af1b4fb6ba72e5  tree.tar.gz
    ou
    b5678ec64d99fe29ff9aa8370c82c8c911252c0e  tree.tar.gz
    ou
    2ddf046b6bcd5a5dd0c163c35e0e09bfba40351f  tree.tar.gz
    ```
    `3f47fc53c320d128f6274c5480af1b4fb6ba72e5`, `b5678ec64d99fe29ff9aa8370c82c8c911252c0e`, `2ddf046b6bcd5a5dd0c163c35e0e09bfba40351f` est une somme de contrôle sur le fichier
    `tree.tar.gz`. Cette somme change si le contenu du fichier est modifié, même y un ajoutant 
    simplement un espace blanc.
- dans la suite de cet examen toute les fois ou l'on vous demande de calculer la somme de contrôle d'un fichier
    `test.txt` par exemple, vous devez lancer la commande: `shasum test.txt`.

<br><br><br><br><br><br>

### Etape 1: Mise en place de l'examen

Positionnez vous dans le répertoire: `/home/rt/examen` si ce n'est pas déjà fait. Decompressez l'archive 
`tree.tar.gz` (qui doit y être) avec la commande suivante: 

```bash
$ tar xzvf tree.tar.gz 
[...]
```

Cette commande crée un répertoire `tree`, qui contient plusieurs répertoires, sous répertoires et fichiers qui 
vont vous servir tout au long de l'examen. __Vous devez toujours vous trouvez dans ce répertoire `tree` et/ou 
ses sous répertoires__

Pour ce faire, faite (__c'est tres important__):
```bash
$ cd /home/rt/examen/tree
```

<br><br><br><br><br><br>

### Etape 2: Pour se faire des points facilement

__QUESTION 1: Dans le répertoire `tree` et ses sous répertoires, combien de fichiers (par oposition à dossier)
ont une taille supérieure ou égale à de 5M (cinq méga-octets) ?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 1
dans le moodle.

<br><br>

__QUESTION 2: Dans le répertoire `tree` et ses sous répertoires, combien de fichiers (par oposition à dossier)
ont une taille inférieurs à de 6k (six kilo-octets) ?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 2
dans le moodle.

<br><br>

__QUESTION 3: Dans les sous-répertoires `tree` dont le nom commence par `M31`
Combien y a-t-il de fichiers (par oposition à dossier) ?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 3
dans le moodle.

<br><br>

__QUESTION 4: Dans le répertoires `tree` et ses sous répertoires,
combien de fichiers s'appellent ou contienne `evaluations` dans leur nom?__

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

<br><br>

__QUESTION 5: Dans le répertoires `tree` et ses sous répertoires,
combien de fichiers ont plus de 89 lignes ?__

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

<br><br>

__QUESTION 6: Dans le répertoires `tree` et ses sous répertoires,
Redirigez dans un fichier `/tmp/q6` le top 4 des fichiers ayant le plus de ligne?__

Calculez la somme de contrôle du fichier `/tmp/q6`. 
La commande pour calculer la somme de contrôle d'un fichier est:

```bash
shasum /tmp/q6
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q6` devra être de la forme:
```bash
395 ./M3401/tp/docs/fichier.dat
125 ./M3104/cours/rfc.txt
90 ./M3314/tp/docs/readme.md
12 ./M3227/tp/support/myfile.txt
```

<br><br>

__QUESTION 7: Dans le répertoires `tree` et ses sous répertoires,
combien de fichiers contiennent le mot `Tahiry`?__

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

<br><br><br><br><br><br>

### Etape 3: Modification des données.

    Les fichiers `evaluations.csv` qui sont contenu dans les répertoires et sous répertoires
    contiennent des lignes de la forme:
    30063651:Tahiry:Razafindralambo:30063651@rt-iut.re:13.00:3.00:19.50
    
    le premier champ est le numéro d'étudiant, le second est le Prénom, le troisième est le nom
    le quatrième est l'adresse email, le 5ieme, 6ieme et 7ieme si ils existent sont des notes.
    
    Les fichiers `evaluations.csv` sous les répertoires `cours` ont une seule note, 
    Les fichiers `evaluations.csv` sous les répertoires `td` ont deux notes, 
    Les fichiers `evaluations.csv` sous les répertoires `tp` ont trois notes. 

    La commande tree vous donne une idée de la structure des répertoires.

__QUESTION 8: Dans tous les fichiers contenant le mot `Tahiry` remplacer les occurances de 
`Tahiry` par `Tahiry N` si le numero d'etudiant est "30063651"?__

(__Cette question n'est pas sur le moodle__)

<br><br>

__QUESTION 9: Mettre dans un fichier `/tmp/q9` toutes les lignes modifiées de la question 8? 
ATTENTION appliquer un sort (sans option) avant de rediriger la sortie dans /tmp/q9__

Calculez la somme de contrôle du fichier `/tmp/q9`. 
La commande pour calculer la somme de contrôle d'un fichier est:

```bash
shasum /tmp/q9
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q9` devra être de la forme:
```bash
[...]
30063651:Tahiry N:Razafindralambo:30063651@rt-iut.re:13.00:5.50
30063651:Tahiry N:Razafindralambo:30063651@rt-iut.re:17.50
30063651:Tahiry N:Razafindralambo:30063651@rt-iut.re:3.50
30063651:Tahiry N:Razafindralambo:30063651@rt-iut.re:6.00:16.00:17.50
[...]
```

<br><br><br><br><br><br>

### Etape 4: Traitement des données.

__Préliminaires__ 

- Positionnez vous dans le sous-répertoire `M3206/cours/notes/` avec la commande

```bash
$ cd M3206/cours/notes/
```

- Dans ce sous répertoire, copier le fichier `evaluations.csv` dans le fichier `moyenne.csv` avec la commande
 
```bash
$ cp evaluations.csv moyenne.csv
```
- Dans cette partie, nous ne travaillons que sur le fichier `M3206/cours/notes/moyenne.csv` que nous venons
    de créer.
- Attention si vous n'avez pas fait les modifications dans l'étape 3 (Question 8), vous  devez les
faire à la main. Editez le fichier `M3206/cours/notes/moyenne.csv` et à la dernière ligne du fichier
remplacer `Tahiry` par `Tahiry N` si ce n'est pas déjà fait.

<br><br>

__QUESTION 10: En partant du fichier `M3206/cours/notes/moyenne.csv`, 
créez un fichier /tmp/q10 contenant la même liste que dans `moyenne.csv`
mais classé dans l'ordre décroissant des notes. ATTENTION: Si il y a deux notes dans le fichier, il faut classer
sur la première,celle se trouvant colonne 5__

Calculez la somme de contrôle du fichier `/tmp/q10`. 
La commande pour calculer la somme de contrôle d'un fichier est:

```bash
shasum /tmp/q10
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q10` devra être de la forme:
```bash
30085542:Severine:Robert:30085542@rt-iut.re:19.50
30082728:Francois-Philippe:Hoareau:30082728@rt-iut.re:19.50
30046179:Ashwin:Malbrouck:30046179@rt-iut.re:19.50
30027414:Chloe:Angama:30027414@rt-iut.re:19.50
30026301:Jordan:Toucoula:30026301@rt-iut.re:19.50
[...]
```

<br><br>

__QUESTION 11: En utilisation le fichier  `M3206/cours/notes/moyenne.csv`,
calculez la moyenne de la classe (somme de toutes les notes divisée par le
nombre d'étudiant)?__

__ATTENTION, calculer la somme de contrôle sur la partie entière de la moyenne__

Si la moyenne est `14.41` alors calculer la somme de contrôle:
```bash
$ echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

<br><br>

__QUESTION 12: En partant du fichier `/tmp/q10`, créez un fichier `/tmp/q12` 
qui contient en début de ligne le classement suivit de ":" (pas de doublon
ni d'ex-aequo)__

Calculez la somme de contrôle du fichier `/tmp/q12`. 
La commande pour calculer la somme de contrôle d'un fichier est:
```bash
shasum /tmp/q12
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q12` devra être de la forme:
```bash
1:30085542:Severine:Robert:30085542@rt-iut.re:19.50
2:30082728:Francois-Philippe:Hoareau:30082728@rt-iut.re:19.50
3:30046179:Ashwin:Malbrouck:30046179@rt-iut.re:19.50
4:30027414:Chloe:Angama:30027414@rt-iut.re:19.50
5:30026301:Jordan:Toucoula:30026301@rt-iut.re:19.50
[...]
```
<br><br>

__QUESTION 13: En partant du fichier `/tmp/q12`, Donnez le classement de `Tahiry N`__


(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

<br><br><br><br><br><br>

### Etape 5: Traitement des notes de TD.

__Préliminaires__ 

- positionnez vous dans le répertoire: `M3206/td/notes/`

__QUESTION 14: A partir du fichier `evaluations.csv` du répertoire `M3206/td/notes/`
créer un fichier `/tmp/q14` qui contient la moyenne de chaque étudiant en td.__


__Attention il y a deux notes dans le fichier `evaluations.csv`.__

__Pour le formatage conserver sur la moyenne seulement la partie entière (sans arrondissement)__

Calculez la somme de contrôle du fichier `/tmp/q14`. 
La commande pour calculer la somme de contrôle d'un fichier est:
```bash
shasum /tmp/q14
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q14` devra être de la forme:
```bash
[...]
30027028:Dylan:Fontaine:30027028@rt-iut.re:12
30048643:Antoine:Gaucherand:30048643@rt-iut.re:14
30087915:Eitcher:Hoareau:30087915@rt-iut.re:2
30088665:David:Huet:30088665@rt-iut.re:8
30012449:Safwaane:Ikbal:30012449@rt-iut.re:13
[...]
```

<br><br><br><br><br><br>

### Etape 6: Traitement des notes de TP.

__Préliminaires__ 

- positionnez vous dans le répertoire: `M3206/tp/notes/`

__QUESTION 15: A partir du fichier `evaluations.csv` du répertoire `M3206/tp/notes/`
créer un fichier /tmp/q15 qui contient la moyenne de chaque étudiant en td.__

__Attention il y a trois notes dans le fichier evaluations.csv.__

__Pour le formatage conserver sur la moyenne seulement la partie entière (sans arrondissement)__

Calculez la somme de contrôle du fichier `/tmp/q15`. 
La commande pour calculer la somme de contrôle d'un fichier est:
```bash
shasum /tmp/q15
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q15` devra être de la forme:
```bash
[...]
30081108:Benoit:Bassonville:30081108@rt-iut.re:13
30001846:Geoffroy:Benoite:30001846@rt-iut.re:14
30016100:Ludovic:Botomisy:30016100@rt-iut.re:15
30091945:Joseph:Boyer:30091945@rt-iut.re:9
30023440:Achille:Brial:30023440@rt-iut.re:11
[...]
```

<br><br><br><br><br><br>

### Etape 7: Moyenne sur toute la matière M3206.

__QUESTION 16: En utilisant les fichiers `/tmp/q15`, `/tmp/q14` et `/tmp/q10` Calculer la moyenne de chaque étudiant. Mettre le résultat dans `/tmp/q16`__


__Attention il y a trois notes une de cours, une de td et une de tp__

__Attention il y a 3 notes mais `Tahiry` n'en a qu'une seule__

__Pour le formatage conserver sur la moyenne seulement la partie entière (sans arrondissement)__

Pour vous aider, vos devez créer une liste des numéros d'étudiant à partir du fichier /tmp/q10.
Vous devez parcourir cette liste et extraire les lignes contenant un numéro d'étudiant.
Vous aurez alors 3 lignes par étudiant sauf (Tahiry), vous effectuez la moyenne
et mettez le résultats dans `/tmp/q16`

Calculez la somme de contrôle du fichier `/tmp/q16`. 
La commande pour calculer la somme de contrôle d'un fichier est:
```bash
shasum /tmp/q16
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q16` devra être de la forme:
```bash
[...]
30043475:Alban-Cedric:Nimba:30043475@rt-iut.re:6
30057640:Aslam:Payet:30057640@rt-iut.re:6
30045653:Jodam:Payet:30045653@rt-iut.re:10
30007339:Arnaud:Quatrehomme:30007339@rt-iut.re:8
30048183:Dorian:Ramouche:30048183@rt-iut.re:9
[...]
```