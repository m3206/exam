#!/bin/bash

cp cours/notes/moyenne.csv mc.csv
cp td/notes/moyenne.csv md.csv
cp tp/notes/moyenne.csv mp.csv

M_TP="mp.csv"
M_TD="md.csv"
M_CR="mc.csv"

liste=$(cat mc.csv | awk 'BEGIN{FS=":"}{print $1}')
rm -fr /tmp/res
touch /tmp/res

#echo $liste
for i in ${liste} ; do
    cat ${M_TP} ${M_TD} ${M_CR} | grep $i > /tmp/t
    cat /tmp/t | awk 'BEGIN {sum = 0; FS=":"}{sum = sum + $5} END { printf("%s:%s:%s:%s:%2.2f\n",$1,$2,$3,$4,sum/3)}' >> /tmp/res
done

rm -fr /tmp/test
rm -fr mc.csv md.csv mp.csv
mv /tmp/res moyenne_generale.csv

