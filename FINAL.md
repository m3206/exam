## Season final TP pour M3206

__Attention, le sujet de l'examen se trouve ici, mais les réponses doivent être
complétées sur le moodle__

__Vérifiez bien que le numéro de réponse correspond au numéro des questions__


>>
__Dans les questions, les deux premières lettres correspondent au début du shasum, 
si vous ne trouver pas le shasum, poster votre ligne de commande. On verra ce que ça donne.__
>>

<br><br><br><br>

### Etape 0: Conseils, outils et notes importantes

- Je vous conseille d'utiliser la machine virtuelle `Debian_Jessie_64bit.vdi` sur RT-CLOUD, 
    dans le répertoire `Machines Virtuelles/Debian/Debian_Minimal_Tahiry`. Ce point est très
    important car l'examen a été conçu avec cette machine. Si vous n'utilisez pas la machine
    `Debian_Jessie_64bit.vdi`, faites le à vos risques et péril. 
- Connectez vous en ssh à votre machine en utilisant `putty` ou un `terminal`. Ce point aussi
    est très important car vous allez devoir faire des copier/coller de vos résultats dans 
    le moodle. Si vous n'utilisez pas ssh, faites le à vos riques et périls.
- Créez un répertoire vide nommé: `/home/rt/examen`. Positionnez vous dans ce répertoire.
- Vous devez dejà avoir l'archive `imdb.tar.gz`, le shasum de cette archive doit être: 
    ```bash
    $ shasum imdb.tar.gz 
    0a7c9affbee296936bf67c259f41d90ecdc549ce  imdb.tar.gz
    ```
- dans la suite de cet examen toute les fois ou l'on vous demande de calculer la somme de contrôle d'un fichier
    `test.txt` par exemple, vous devez lancer la commande: `shasum test.txt`.

<br><br><br><br><br>

### Etape 1: Mise en place de l'examen

Positionnez vous dans le répertoire: `/home/rt/examen` si ce n'est pas déjà fait. Decompressez l'archive 
`imdb.tar.gz` (qui doit y être) avec la commande suivante: 

```bash
$ tar xzvf imdb.tar.gz 
[...]
```

Cette commande crée un répertoire `imdb`, qui contient plusieurs répertoires, sous répertoires et fichiers qui 
vont vous servir tout au long de l'examen. __Vous devez toujours vous trouvez dans ce répertoire `imdb` et/ou 
ses sous répertoires__

Pour ce faire, faite (__c'est très important__):
```bash
$ cd /home/rt/examen/imdb
```

<br><br><br><br><br><br>

### Etape 2: Echauffement.

__QUESTION 1: Dans le répertoire `imdb` et ses sous répertoires, combien de fichiers (par opposition à dossier)
ont une taille supérieure ou égale à de 10M (dix méga-octets) et dont le nom contient la chaine de caractère `10` ?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 1
dans le moodle.

>>
Utiliser la commande find avec les options de taille, de nom de type.
>>

<br><br>

__QUESTION 2: Dans le répertoire `imdb` et ses sous répertoires, combien de fichiers (par oposition à dossier)
ont une taille supérieure à de 5M (5 méga-octets) et inférieure 15M (15 méga-octets)?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 2
dans le moodle.

>>
Utiliser la commande find avec les options de taille et de type.
>>

<br><br>

__QUESTION 3: Dans les sous-répertoires `imdb` dont le nom commence par `act`
Combien y a-t-il de fichiers (par oposition à dossier) ?__

Si votre réponse est par exemple `14`, dans un terminal lancer la commande: `echo 14 | shasum`
```bash
echo 14 | shasum
030514d80869744a4e2f60d2fd37d6081f5ed01a  -
```
Et copiez la somme de contrôle (dans notre exemple) `030514d80869744a4e2f60d2fd37d6081f5ed01a` pour la réponse à la question 3
dans le moodle.

>>
Vous avez plusieurs choix. 
- Imbriquer deux commandes find.
- Faire ls avec les bonnes options dans chaque répertoire, rediriger le résultat dans un fichier et compter le nombre de ligne de ce fichier. Attention aux ajouts inutiles que peut faire la commande ls.
- Compter à la main (pas recommandé, mais fonctionne bien aussi).
- Générer les shasum de tous les nombres, et vérifier avec l'indice donné dans le mooddle. :-)
>>

<br><br>

__QUESTION 4: Dans le répertoires `imdb` et ses sous répertoires,
combien de fichiers s'appellent ou contiennent__
- `15` ET `re` dans leur nom 
OU
- `17` ET `or` dans leur nom ?

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

>>
Vous avez plusieurs choix. 
- Soit le faire en une commande find avec l'utilisation du OU logique. Il faut trouver la bonne option
- Soit effectuer deux recherches. Dans ce cas faites attention aux doublons. (je ne suis pas sur qu'il y en ai)
>>

<br><br>

__QUESTION 5: Dans le répertoires `imdb` et ses sous répertoires,
combien de fichiers ont plus de 100000 lignes ?__

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

>>
Vous savez comment faire. Pour eviter d'avoir un total sur la commande wc, il vaut mieux l'executer dans la commande find. 
>>

<br><br>

__QUESTION 6: Dans le répertoires `imdb` et ses sous répertoires,
redirigez dans un fichier `/tmp/q6` le top 15 des fichiers ayant le plus de ligne?__

Calculez la somme de contrôle du fichier `/tmp/q6`. 
La commande pour calculer la somme de contrôle d'un fichier est:

```bash
shasum /tmp/q6
```
(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q6` devra être de la forme __Attention à l'ordre décroissant__:
```bash
2212800 ./pays/countries.list.sanitized   
591190 ./films/movies_524590.list   
514452 ./films/movies_571073.list   
475627 ./films/movies_473927.list   
427890 ./films/movies_425790.list   
4000 ./films/movies_406200.list
```

__Vérifier que vous n'avez pas d'espace en début de fichier et que le format est exactement le même__

>>
Utiliser la réponse de la question précédente, et ordonner la sortie. Faites attention au format. Utiliser awk pour gérer le problème de formatage
ou faites le à la main (il n'y a que 15 lignes).
>>

<br><br>

__QUESTION 7: Dans le répertoires `imdb` et ses sous répertoires,
quels sont les fichiers contenant la chaine de caractère `Jolie, Angelina`? Rediriger cette liste dans /tmp/q7 dans l'ordre alphabétique__

```bash
shasum /tmp/q7
```

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q7` devra être de la forme. __Attention à l'ordre alphabétique du répertoire__:

```bash
./actrices/actresses_1.list
./directeurs/directors_16.list 
./producteurs/producers_393.list 
```

>>
Normalementil n'y a pas de difficulté ici. Utilisé les bonnes options de grep. 
>>

<br><br><br><br><br><br>

### Etape 3: Mon film préféré.


__QUESTION 8: Le scénariste de mon film préféré est: `Tarantino, Quentin`. Trouver 
la liste des films scénarisé par `Tarantino, Quentin` (dans le dossier scénaristes)
et rediriger cette liste dans /tmp/q8. Compter le nombre de ligne de /tmp/q8 pour avoir
le nombre de film scénarisé par `Tarantino, Quentin`. Poster sur moodle le shasum du nombre
de film scénarisé par `Tarantino, Quentin`?__

Le contenu d'un fichier scénariste est le suivant:
```bash
NAME: Lucas, George
FILM: Film 1 (2016)
FILM: Un autre film (2006)
NAME: Lucas, Henri
```
dans cet exemple, `Lucas, George` a scénarisé 2 films. 

Le fichier `/tmp/q8` est de la forme:
```
FILM:: Leonard Cohen: Dance Me to the End of Love (1995)
FILM:: Love Birds in Bondage (1983)
FILM:: My Best Friend's Birthday (1987) (written by)
FILM:: Natural Born Killers (1994) (story) <1,1,1>
FILM:: Pulp Fiction (1994) (stories by) <1,1,1>
```

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)
 

>>
Afficher toutes les lignes de tous les fichiers. Selectionner avec sed les lignes 
se trouvant entre Tarantino, Quentin et NAME (qui est le scénariste suivant dans
la liste). N'oublié pas d'enlever les noms de Tarantino, Quentin et du scénariste
suivant.
>>


<br><br>

__QUESTION 9: Mon film préféré a plusieurs producteurs et co-producteurs dont: 
`Bender, Lawrence` qui est (`producer`). Combien de film a
produit `Bender, Lawrence` (`producer` ou `executive producer`). Pour cela, rediriger la liste des films produit dans /tmp/q9 et compter
le nombre de ligne de ce fichier.?__

Le contenu d'un fichier dans producteur est le suivant:
```bash
NAME: Lucas, George
FILM: Film 1 (2016) (producer)
FILM: Un autre film (2006) (co-producer)
NAME: Lucas, Henri
```

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q9` devra être de la forme:
```bash
[...]
FILM:: Killing Zoe (1993) (executive producer)
FILM:: Killshot (2008) (producer)
FILM:: Knockaround Guys (2001) (producer)
FILM:: Long Island Confidential (2008) (TV) (executive producer)
FILM:: Lost in Oz (2002) (TV) (executive producer)
[...]
```

>>
Faites comme pour la question précédente mais cette fois vous devez affiner la recherche
avec le mot clé producer
>>

<br><br>

__QUESTION 10: Mon film préféré a plusieurs co-producteurs dont: `Hellerman, Paul` qui est  `(co-producer)`. Combien de film a
produit `Hellerman, Paul` (`co-producer`). Pour cela, rediriger la liste des films produit dans /tmp/q10 et compter
le nombre de ligne de ce fichier.?__

(postez sur le moodle la somme de contôle de votre réponse, comme pour les questions précédentes)

Pour vous aider, le fichier `/tmp/q10` devra être même forme que `/tmp/q9` 

>>
Faites comme pour la question précédent mais cette fois vous devez affiner la recherche
avec le mot clé co-producer
>>
<br><br>

__QUESTION 11: Dans les fichiers `/tmp/q8`, `/tmp/q9` et `/tmp/q10` vous avez tous les films scénarisé par `Tarantino, Quentin`
produit par `Bender, Lawrence` et co-produit par `Hellerman, Paul`.__ Pour resserer la recherche, vous devez:
- Isoler le nom du film et ne garder que la partie qui vient avant la date
- Mettre dans l'ordre alphabetique
- Supprimer les doublons
- Enlever `FILM:: ` en début de chaque ligne.

Rédiriger la liste ainsi simplifié dans `/tmp/q11`. Donner le shasum du fichier `/tmp/q11`

```bash
$ shasum /tmp/q11
cdd12...
```
>>
Il y a plusieurs manière de faire. Afficher les 3 fichiers à la suite. 
Garder la partie de la ligne se trouvant avant "(". Vous pouvez utiliser 
awk (ou cut). Mettre dans l'ordre alphabétique et ne garder que les doublons. Enlever
"FILM :: " avec awk, sed ou cut.
>>


<br><br>

__QUESTION 12: Une partie de mon film préféré à été tourné à `Carson, California`. Vous pouvez
trouver tous les lieux ou un films a été tourné dans les fichiers se trouvant dans le repertoire `lieu`. A 
partir du fichier `/tmp/q11` et du lieu de tournage, vous devez pouvoir trouver mon film préféré__

Il est sous la forme: `Mon Film (1999)`

Faites:
```bash
echo "Mon Film (1999)" | shasum
```

Et renseigner le résultat dans moodle.

>>
Vous devez parcourir la liste fourni à la question précédente. Chaque ligne de cette liste permettra de filtrer
la sortie avec un grep et vous refaites un autre grep avec le lieu.
>>

<br><br><br><br><br><br>

### Etape 4: Bonus.

__QUESTION 13: Ecrire un script qui automatise la recherche.__

On lancera en ligne de commande:
```bash
./trouve.sh --actor "Sheen, Martin" --director "Coppola, Francis" --writer "Milius, John" --location "Laguna, Philippines"
```

Trouver le film dont:
- un acteur est: "Sheen, Martin"
- le directeur est: "Coppola, Francis"
- le scénariste est: "Milius, John"
- Tourné à: "Laguna, Philippines"

Le résultat est un film sous la forme: `Mon Film (1999)`

Faites:
```bash
echo "Mon Film (1999)" | shasum
```

Et renseigner le résultat dans moodle.

__QUESTION 14: Copier le script écrit à la question 13.__

>>
On your own.
>>
