#!/bin/bash


dir=$(find . -maxdepth 1 -type d)
for d in ${dir}; do
    convert -size 200x200 -gravity center -background white -fill black label:"$d" ${d}.png
    mv ${d}.png ${d}/cours/docs/.
done

dir=$(find . -maxdepth 1 -type d | cut -f 2 -d"/")
for d in ${dir}; do
    convert xc:none -page A4 cours_01_${d}.pdf
    convert xc:none -page A4 cours_02_${d}.pdf
    #echo cours_01_${d}.pdf
    mv cours_01_${d}.pdf ${d}/cours/supports/.
    mv cours_02_${d}.pdf ${d}/cours/supports/.
done

##convert -size 200x200 -gravity center -background white -fill black label:"text goes here" canvas.png
